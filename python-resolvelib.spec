%global pypi_name resolvelib

Summary:        Resolve abstract dependencies into concrete ones
Name:           python-%{pypi_name}
Version:        1.0.1
Release:        2%{?dist}
License:        ISC
URL:            https://github.com/sarugaku/resolvelib
Source0:        %{url}/archive/refs/tags/%{version}.tar.gz

Patch3000:      remove-commentjson-dep.patch

BuildRequires:  python3-devel
BuildRequires:  python3dist(setuptools)
BuildArch:      noarch

%description
ResolveLib at the highest level provides a Resolver class that
includes dependency resolution logic. You give it some things, and a little
information on how it should interact with them, and it will spit out a
resolution result. Intended Usage :: import resolvelib Things I want to
resolve. requirements [...] Implement logic so the resolver understands the
requirement format. class...


%package -n     python3-%{pypi_name}
Summary:        Resolve abstract dependencies into concrete ones
Provides:       python3-%{pypi_name}

%description -n python3-%{pypi_name}
ResolveLib at the highest level provides a Resolver class that
includes dependency resolution logic. You give it some things, and a little
information on how it should interact with them, and it will spit out a
resolution result. Intended Usage :: import resolvelib Things I want to
resolve. requirements [...] Implement logic so the resolver understands the
requirement format. class...


%prep
%autosetup -n %{pypi_name}-%{version} -p1
rm -rf %{pypi_name}.egg-info

%generate_buildrequires
%pyproject_buildrequires -x test

%build
%py3_build

%install
%py3_install

%check
%pytest -v

%files -n python3-%{pypi_name}
%license LICENSE
%doc README.rst
%{python3_sitelib}/%{pypi_name}
%{python3_sitelib}/%{pypi_name}-%{version}-py%{python3_version}.egg-info

%changelog
* Tue May 14 2024 Shuo Wang <abushwang@tencent.com> - 1.0.1-2
- enable %check for python-resolvelib

* Mon Sep 25 2023 Shuo Wang <abushwang@tencent.com> - 1.0.1-1
- update to 1.0.1

* Tue Sep 19 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 0.5.5-6
- Rebuilt for python 3.11

* Fri Sep 08 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 0.5.5-5
- Rebuilt for OpenCloudOS Stream 23.09

* Fri Apr 28 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 0.5.5-4
- Rebuilt for OpenCloudOS Stream 23.05

* Fri Mar 31 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 0.5.5-3
- Rebuilt for OpenCloudOS Stream 23

* Thu Feb 23 2023 Miaojun Dong <zoedong@tencent.com> - 0.5.5-2
- remove README.md

* Tue Nov 29 2022 rockerzhu <rockerzhu@tencent.com> - 0.5.5-1
- Initial build
